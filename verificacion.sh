#!/bin/bash

# This scripts does a simple comparison of two files
# using diff cmd

# Variable to check for errors
error=0

seed=$RANDOM

# Multiplicación de vectores y matrices
echo "Evaluacion para Multiplicación de vectores y matrices"
echo " "
make run_multi SEED=seed > results_multi.txt
make run_multi_pthread SEED=seed > results_multi_pthread.txt
diff results_multi.txt results_multi_pthread.txt > diff.txt
echo "Diferencias en la Multiplicación de vectores y matrices"
cat diff.txt
a=`wc -l diff.txt| cut -f1 -d ' '`
if (($a == 0)); then
   echo "Resultados son correctos"
else
  echo "Resultados son incorrectos"
   error=1
fi

rm ./*.txt
# Calculo de PI
echo " "
echo "Evaluacion para el calculo de PI"
echo " "
make run_PI > results_PI.txt
make run_PI_pthread > results_PI_pthread.txt
diff results_PI.txt results_PI_pthread.txt > diff.txt
echo "Diferencias en el calculo de PI"
cat diff.txt
a=`wc -l diff.txt| cut -f1 -d ' '`
if (($a == 0)); then
   echo "Resultados son correctos"
else
  echo "Resultados son incorrectos"
   error=1
fi

rm ./*.txt