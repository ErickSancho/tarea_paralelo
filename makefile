SRC = ./src
BIN = ./bin

## Flags ##
CC = gcc
CFLAGS = -pthread -lm -Wall -pedantic
EXE_PI_normal = ./$(BIN)/Ejecutable_PI_normal
EXE_PI_pthread = ./$(BIN)/Ejecutable_PI_pthread
EXE_multi_normal = ./$(BIN)/Ejecutable_multi_normal
EXE_multi_pthread = ./$(BIN)/Ejecutable_multi_pthread

####Seed para multi###
SEED = 0

## Targets ##

compilar: compilar_multi compilar_multi_pthread compilar_PI compilar_PI_pthread

compilar_PI: ./src/Calculo_PI.c
	@mkdir -p $(BIN)
	@$(CC) $? $(CFLAGS) -o $(EXE_PI_normal)

run_PI: compilar_PI
	@$(EXE_PI_normal)

######### 

compilar_PI_pthread: ./src/Calculo_PI_pthread.c
	@mkdir -p $(BIN)
	@$(CC) $? $(CFLAGS) -o $(EXE_PI_pthread)

run_PI_pthread: compilar_PI_pthread
	@$(EXE_PI_pthread)

#########

compilar_multi: ./src/mult_matrix.c
	@mkdir -p $(BIN)
	@$(CC) $? $(CFLAGS) -o $(EXE_multi_normal)

run_multi: compilar_multi
	@$(EXE_multi_normal) $(SEED)

######### 

compilar_multi_pthread: ./src/mult_matrix_pthread.c
	@mkdir -p $(BIN)
	@$(CC) $? $(CFLAGS) -o $(EXE_multi_pthread)

run_multi_pthread: compilar_multi_pthread
	@$(EXE_multi_pthread) $(SEED)

#########

clean:
	@rm -rfv $(BIN)