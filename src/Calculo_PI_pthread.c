#include<stdlib.h>
#include<stdio.h>
#include<math.h>
#include<pthread.h>
#include <time.h>

static pthread_mutex_t mutex;

long double PI1 = 0;

static long int maximo = 10000000;

void calculo_PI_pthread();

void *rutina_par(void *unused);

void *rutina_impar(void *unused);



int main(int argc, char const *argv[])
{   
    calculo_PI_pthread();
    printf("El valor aproximado de PI es de: %0.10lf\n", (double)PI1); 
    return 0;
}



/*Calculo de Pi de empleando pthread*/
void calculo_PI_pthread()
{
    pthread_t thread1, thread2;

    pthread_attr_t attr;

    pthread_attr_init(&attr);

    if(0!=pthread_create(&thread1, &attr, rutina_par, NULL))
    {
        printf("Error creando thread1\n");
        exit(1);
    }

    if(0!=pthread_create(&thread2, NULL, rutina_impar, NULL))
    {
        printf("Error creando thread2\n");
        exit(1);
    }

    pthread_join(thread1, NULL);
    pthread_join(thread2, NULL);
}

// 3.1415926526  Valor real
// 3.1415926526  Valor pi normal
// 3.1415926526  Valor pi con threads



void *rutina_par(void *unused){
    for (long int i = 0 ;i<maximo;i=i+2)
    {
        pthread_mutex_lock(&mutex);
        long double iteracion = 4 * pow(-1,i) / (2 * i + 1);
        PI1 = PI1 + iteracion;
        pthread_mutex_unlock(&mutex);
        
    }
    return 0;
}

void *rutina_impar(void *unused)
{
    for (long int j = 1 ;j<maximo;j=j+2)
    {
        pthread_mutex_lock(&mutex);
        long double iteracion = 4 * pow(-1,j) / (2 * j + 1);
        PI1 = PI1 + iteracion;
        pthread_mutex_unlock(&mutex);
        
    }
    return 0;
}
