#include<stdlib.h>
#include<stdio.h>
#include<math.h>
#include <time.h>
#include <pthread.h>

void matrix_calculator_pthread(int n, int(*matriz)[n], int* vector);
void *rutina_thread(void *threadarg);

// Struct para pasar datos a cada thread
struct thread_data{
   int thread_id;
   int* vector_a;
   int* matrix_row;
   int n;
   int* result;
};

int main(int argc, char const *argv[])
{
    // extraccion de semilla:
    int seed = atoi(argv[1]);

    // clock_t t_ini, t_fin;
    // double secs;
    // t_ini = clock();
    // Creacion de matriz y vector de tamaños random con numeros random
    srand ( seed );
    int n=rand()%6+3;
    int matriz_a[n][n]; 
    int vector_a[n];
    for (int i=0; i<n; i++){
        for (int j =0; j<n; j++){
            matriz_a[i][j] = rand()%10;
        }
        vector_a[i] = rand()%10;
    }
    // Funcion para calcular el resultado de la multiplicacion
    // de la matriz nxn con el vector nx1
    matrix_calculator_pthread(n,matriz_a,vector_a);
    // t_fin = clock();
    // secs = (double)(t_fin - t_ini) / CLOCKS_PER_SEC;
    // printf("%.16g segundos\n", secs);
    return 0;
}

// Se calcula el resultado de la multiplicacion de una matriz nxn
// con un vector nx1, para esto se usan threads, en cuestion se 
// usan n threads que haran la multiplicacion de cada fila 
// de la matriz con el vector. Recibe como parametro la matriz,
// el vector y el valor de n.
void matrix_calculator_pthread(int n, int(*matriz)[n], int* vector)
{   
    int NUM_THREADS = n;
    int result[n];
    struct thread_data thread_data_array[NUM_THREADS];
    pthread_t threads[NUM_THREADS];
    int rc;
    for(int t=0; t<NUM_THREADS; t++){
        thread_data_array[t].thread_id = t;
        thread_data_array[t].vector_a=vector;
        thread_data_array[t].matrix_row = matriz[t]; 
        thread_data_array[t].n = n;
        thread_data_array[t].result = result;
        // printf("Creando thread %d\n", t);
        rc = pthread_create(&threads[t], NULL, rutina_thread, &thread_data_array[t]);
        if (rc){
            printf("Error el codgio pthread_create() retorno %d\n", rc);
            exit(-1);
        } 
        else{
            pthread_join(threads[t], NULL);
        }
    }
    printf("\nmatriz nxn \n");
    for (int i=0; i<n; i++){
        for (int j =0; j<n; j++){
             printf("%d ", matriz[i][j]);
        }
        printf("\n");
    }
    printf("\nvector nx1 \n");
    for (int i=0; i<n; i++){    
        printf("%d \n", vector[i]);
    }
    printf("\nResultado de la multiplicacion: \n");
    for (int i=0; i<n; i++){    
        printf("%d \n", result[i]);
    }
    printf("\n");
    pthread_exit(NULL);
}

void *rutina_thread(void *threadarg){
    struct thread_data *data;
    data = (struct thread_data *) threadarg;
    int result_a = 0;
    int* vector = data->vector_a;
    int* matrix_row_i = data->matrix_row;
    for (int i =0; i<data->n; i++){
        result_a += vector[i] * matrix_row_i[i];
    }
    data->result[data->thread_id] = result_a;
    pthread_exit(NULL);
}