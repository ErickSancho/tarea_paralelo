#include<stdlib.h>
#include<stdio.h>
#include<math.h>
#include <time.h>

void matrix_calculator(int n, int(*matriz)[n], int* vector);

int main(int argc, char const *argv[])
{
    // extraccion de semilla:
    int seed = atoi(argv[1]);

    // clock_t t_ini, t_fin;
    // double secs;
    // t_ini = clock();
    // Creacion de matriz y vector de tamaños random con numeros random
    srand ( seed );
    int n=rand()%6+3;
    int matriz_a[n][n];
    int vector_a[n];
    for (int i=0; i<n; i++){
        for (int j =0; j<n; j++){
            matriz_a[i][j] = rand()%10;
        }
        vector_a[i] = rand()%10;
    }
    // Funcion para calcular el resultado de la multiplicacion
    // de la matriz nxn con el vector nx1
    matrix_calculator(n,matriz_a,vector_a);
    // t_fin = clock();
    // secs = (double)(t_fin - t_ini) / CLOCKS_PER_SEC;
    // printf("%.16g segundos\n", secs);
    return 0;
}

// Se calcula el resultado de la multiplicacion de una matriz nxn
// con un vector nx1, para esto se multiplica cada fila de la matriz
// por el vector y se guarda el resultado en un array.
// Recibe como parametro la matriz, el vector y el valor de n.
void matrix_calculator(int n, int(*matriz)[n], int* vector)
{   
    int result[n];
    for (int i=0; i<n; i++){
        int result_a = 0;
        for (int j =0; j<n; j++){
            result_a += vector[j]*(int)matriz[i][j];
        }
        result[i] = result_a;
    }
    printf("\nmatriz nxn \n");
    for (int i=0; i<n; i++){
        for (int j =0; j<n; j++){
             printf("%d ", matriz[i][j]);
        }
        printf("\n");
    }
    printf("\nvector nx1 \n");
    for (int i=0; i<n; i++){    
        printf("%d \n", vector[i]);
    }
    printf("\nResultado de la multiplicacion: \n");
    for (int i=0; i<n; i++){    
        printf("%d \n", result[i]);
    }
    printf("\n");

}

