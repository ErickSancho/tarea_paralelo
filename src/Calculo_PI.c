#include<stdlib.h>
#include<stdio.h>
#include<math.h>
#include <time.h>

void calculo_PI_Normal();



int main(int argc, char const *argv[])
{
    calculo_PI_Normal();
    return 0;
}

/*Calculo de Pi de manera normal*/
void calculo_PI_Normal()
{
    long int maximo = 10000000;
    long double PI=0;

    for (long int i = 0;i<maximo;i++)
    {
        double iteracion = 4 * pow(-1,i) / (2 * i + 1);
        PI = PI + iteracion;
    }

    printf("El valor aproximado de PI es de: %0.10lf\n", (double)PI);  
}

